package com.example.coyote.core.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.coyote.AppApplication

/**
 * BaseFragment
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */
class BaseFragment : Fragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.inject(this)
        super.onCreate(savedInstanceState)
    }
}