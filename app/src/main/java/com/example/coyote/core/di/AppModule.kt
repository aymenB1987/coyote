package com.example.coyote.core.di

import android.app.Application
import android.content.Context
import com.example.coyote.core.di.module.*
import dagger.Binds
import dagger.Module

/**
 * AppModule
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */
@Module(
    includes = [RemoteModule::class,RepositoryModule::class, RetrofitModule::class, ViewModelModule::class, RoomModule::class]
)
abstract class AppModule {
    @Binds
    abstract fun provideContext(application: Application): Context
}