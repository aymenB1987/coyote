package com.example.coyote.core.di.injector

import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector

/**
 * AppModule
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */

interface HasFragmentXInjector {
    fun fragmentXInjector(): AndroidInjector<Fragment>
}