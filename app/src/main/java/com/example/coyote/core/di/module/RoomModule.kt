package com.example.coyote.core.di.module

import android.content.Context
import androidx.room.Room
import com.example.coyote.BuildConfig
import com.example.coyote.core.db.CoyoteDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * RoomtModule
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */

@Module
class RoomModule {

    @Provides
    @Singleton
    fun providesRoom(appContext : Context) : CoyoteDataBase {
        return Room
            .databaseBuilder(appContext, CoyoteDataBase::class.java, BuildConfig.COYOTE_DB)
            .fallbackToDestructiveMigration()
            .build()
    }

//    @Singleton
//    @Provides
//    fun provideUserDao(userDatabase: UsersDatabase): UsersDao {
//        return userDatabase.usersDao()
//    }

}