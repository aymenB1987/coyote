package com.example.coyote.core.di

import android.app.Application
import com.example.coyote.AppApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


/**
 * AppComponent
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule :: class,
        AppModule :: class,
        BuilderModule :: class
    ]
)
interface AppComponent : AndroidInjector<AppApplication>{
    @Component.Builder
    interface  Builder {
        @BindsInstance
        fun application (application : Application): Builder

        fun build():AppComponent
    }
}