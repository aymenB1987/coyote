package com.example.coyote.core.db
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import retrofit2.Converter

@Database(entities = [],version = 1,exportSchema = false)
abstract class CoyoteDataBase : RoomDatabase() {
}