package com.example.coyote.core.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.coyote.AppApplication


/**
 * BaseActivity
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */
class BaseActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.inject(this)
        super.onCreate(savedInstanceState)
    }
}