package com.example.coyote.core.di

import com.example.coyote.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * BuilderModule
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */
@Module
abstract class BuilderModule {

    // =============================================================================================
    // ACTIVITY
    // =============================================================================================

    @ContributesAndroidInjector
    abstract fun injectMainActivity() : MainActivity



    // =============================================================================================
    // FRAGMENT
    // =============================================================================================
}