package com.example.coyote.core.di.module

import androidx.lifecycle.ViewModelProvider
import com.example.coyote.core.di.factory.ViewModelFactory
import dagger.Binds
import dagger.Module

/**
 * VIewModelModule
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}