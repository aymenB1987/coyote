package com.example.coyote.features.users.domain.usecases

import com.example.coyote.features.users.domain.entities.User
import com.example.coyote.features.users.domain.repositories.ListUserRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * GetListUserCase
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
class GetListUser @Inject constructor(var listUserRepository: ListUserRepository) {
    operator fun invoke(): Single<List<User>> = listUserRepository.getListUser()
}