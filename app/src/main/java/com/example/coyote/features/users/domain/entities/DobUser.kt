package com.example.coyote.features.users.domain.entities

/**
 * DobUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */

interface DobUser {

    fun getDate(): String
    fun getAge() : Int
}