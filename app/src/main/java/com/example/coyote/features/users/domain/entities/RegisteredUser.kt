package com.example.coyote.features.users.domain.entities
/**
 * RegisteredUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface RegisteredUser {
    fun getDate(): String
    fun getAge() : Int
}