package com.example.coyote.features.users.domain.entities

/**
 * PictureUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface PictureUser {
    fun getLarge(): String
    fun getMedium(): String
    fun getThumbnail(): String

}