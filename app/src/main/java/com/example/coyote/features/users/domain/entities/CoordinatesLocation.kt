package com.example.coyote.features.users.domain.entities

/**
 * CordiantesLocationEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface CoordinatesLocation {


    fun getLatitude() : Double
    fun getLongitude() : Double
}