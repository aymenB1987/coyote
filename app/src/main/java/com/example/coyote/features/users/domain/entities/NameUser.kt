package com.example.coyote.features.users.domain.entities

/**
 * NameUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface NameUser {
    fun getTitle() : String
    fun getFirst() : String
    fun getLast() : String
}