package com.example.coyote.features.users.domain.entities

/**
 * LocationUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */


interface LocationUser {
    fun getStreet() : StreetLocation
    fun getCity() : String
    fun getState() : String
    fun getCountry() : String
    fun getPostCode() : String
    fun getCordinates() : CoordinatesLocation
    fun getTimeZone() : TimezoneLocation


}