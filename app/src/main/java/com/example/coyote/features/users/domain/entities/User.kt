package com.example.coyote.features.users.domain.entities


/**
 * PictureUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface User {

    fun getGender(): String
    fun getName():NameUser
    fun getLocation():LocationUser
    fun getEmail(): String
    fun getLogin():LoginUser
    fun getDobUser():DobUser
    fun getRegisterdUser():RegisteredUser
    fun getPhone():String
    fun getCell():String
    fun getIdUser():IdUser
    fun getPicture():PictureUser
    fun getNat():String


}