package com.example.coyote.features.users.domain.repositories

import com.example.coyote.features.users.domain.entities.User
import io.reactivex.Single

/**
 * ListUserRepository
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface ListUserRepository {
    fun getListUser(): Single<List<User>>
}