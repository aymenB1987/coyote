package com.example.coyote.features.users.domain.entities

/**
 * LoginUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface LoginUser {
    fun getUuid(): String
    fun getUsername() : String
    fun getPassword() : String
    fun getSalt() : String
    fun getMd5() : String
    fun getSha1() : String
    fun getSha256() : String

}