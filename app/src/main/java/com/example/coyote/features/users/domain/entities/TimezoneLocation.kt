package com.example.coyote.features.users.domain.entities

/**
 * TimezoneLocationEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */

interface TimezoneLocation {

    fun getOffset() : Double
    fun getDescription() : String
}