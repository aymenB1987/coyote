package com.example.coyote.features.users.domain.entities

/**
 * StreetLocationEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */

interface StreetLocation {

    fun getNumber() : Int
    fun getNameStreet() : String
}