package com.example.coyote.features.users.domain.entities

/**
 * IdUserEntity
 * Aymen Bouali
 * Coyote
 * @date 18/09/2020
 */
interface IdUser {
    fun getName(): String
    fun getValue() : String
}