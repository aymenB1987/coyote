package com.example.coyote

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.multidex.MultiDex
import com.example.coyote.core.di.DaggerAppComponent
import com.example.coyote.core.di.injector.HasFragmentXInjector
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import timber.log.Timber
import javax.inject.Inject


/**
 * AppApplication
 * Aymen Bouali
 * Coyote
 * @date 17/09/2020
 */

class AppApplication : DaggerApplication(), HasFragmentXInjector {
    @Inject
    lateinit var fragmentXInjector: DispatchingAndroidInjector<Fragment>


    // =============================================================================================
    // LIFECYCLE
    // =============================================================================================
    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        // -- Timber
        Timber.plant(Timber.DebugTree())
    }

    // =============================================================================================
    // DAGGER OVERRIDE
    // =============================================================================================
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.builder()
            .application(this)
            .build()
    override fun fragmentXInjector(): AndroidInjector<Fragment> = fragmentXInjector


    // =============================================================================================
    // COMPANION
    // =============================================================================================
    companion object {

        lateinit var app: AppApplication

        fun inject(activity: Activity) = AndroidInjection.inject(activity)

        fun inject(fragment: Fragment) {
            checkNotNull(fragment) {
                "androidx.fragment.app.Fragment"
            }
            app = fragment.activity!!.application as AppApplication
            app.fragmentXInjector.inject(fragment)
        }
    }
}