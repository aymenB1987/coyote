# Coyote

Display list of users from the Api

## Get started
this project is a test for Coyote where i will develope an Android application (kotlin)

## TO DO
- [+] Prepare the archtitecture of project.
- [+] Integrate Dagger in application.
- [+] Create the  basics component (Activity,Fragment) and modules for project.
- [+] Intgrate Retrofit and Room.
- [ ] Create the domain layer for user list.
- [ ] Create and Test the data layer.
- [ ] Create User Interface.
- [ ] Create the pagination and the adapter for list.
- [ ] Create and implement the ViewModel.